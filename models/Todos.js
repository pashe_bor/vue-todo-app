const mongoose = require('mongoose');

const TodosSchema = new mongoose.Schema({
  title: String,
  text: String,
  user: String,
  importance: String,
  date: String
}, {collection: 'todos'});

const Todos = mongoose.model('Todos', TodosSchema);

module.exports = Todos;