const express = require('express'),
  router = express.Router(),
  dateFormat = require('./../utils/format.date');
const Todos = require('./../models/Todos');

router.get('/todos', function (request, response) {
  if (request.session.user) {
    Todos.find( {'user': request.session.user._id} ,function (error, todos) {
      if (error) return handleError(error).send(error);
      response.send({response: todos});
    }).sort({_id: -1})
  } else {
    response.send({response: false})
  }
});
//Adding a todo
router.put('/todos', function (request, response) {
  let todoData = request.body, now = new Date();
  todoData.user = request.session.user._id;
  todoData.date = dateFormat(now);
  
  const todo = new Todos(todoData);
  todo.save(function (error, docs) {
    if (error) return handleError(error).send({response: error});
    response.send({response: docs})
  })
});
//Delete todo
router.delete('/todos/:id', function (request, response) {
    let id = request.params.id;
    Todos.findByIdAndRemove({_id: id}, function (error, todo) {
        if (error) return response.status(500).send(error);
        response.send({response: todo});
    })


});
//Update todo
router.post('/todos', function (request, response) {
  let formData = request.body;
  Todos.findByIdAndUpdate(
    formData.id,
    {text: formData.text, title: formData.title, importance: formData.importance},
    {new: true}, function (error, todo) {
      if (error) return response.status(500).send(error);
      console.log(todo);
      response.send({response: todo})
    })
});


module.exports = router;