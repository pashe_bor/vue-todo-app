const express = require('express'),
  router = express.Router();
const User = require('./../models/Users');
const upload = require('./../utils/file.uploader');

router.get('/users', function (request, response) {
  if (request.session.user) {
    response.send({response: request.session.user})
  } else {
    response.send({response: false})
  }
});

router.delete('/users/:id', function (request, response) {
  let id = request.params.id;
  User.findByIdAndRemove(id, function (error, user) {
    if (error) return response.status(500).send(error);
    const res = {
      message: "User " + user.name + " successfully deleted",
      id: user._id
    };
    response.send(res);
  })
});

router.put('/users', function (request, response) {
  
    upload(request, response, function (error) {
      if (error) console.log(error);
      let userData = request.body;
      if (request.file) userData.avatar = request.file.filename;
      
      if (userData.password === userData.passwordConfirm) {
        let user = new User(userData);
        user.save(function (error, docs) {
          if (error) return handleError(error).send(error);
          response.send({response: true});
        });
      } else {
        response.send({response: false});
      }
    })
});

router.post('/users', function (request, response) {
  let loginUser = request.body;
  User.findOne(
    {'email': loginUser.email},
    function (error, users) {
      let user = users;
      users.comparePassword(loginUser.password, function(err, isMatch) {
        if (err) throw err;
        if (!isMatch) {
           response.send({response: false})
        } else {
          request.session.user = user; //Setting user session
          response.send({response: user});
        }
      });
  });

})

//Update user
router.post('/user-update', function (request, response) {
  upload(request, response, function (error) {
    if (error) console.log(error);
    let editadData = request.body;
    console.log(request.file);
    if (typeof request.file !== 'undefined') {
      editadData.avatar = request.file.filename;
    } else {
      editadData.avatar = '';
    }
    User.findByIdAndUpdate(
      request.session.user._id,
      {
        avatar: editadData.avatar,
        name: editadData.name,
        email: editadData.email
      },
      {new: true},
      function (error, user) {
        if (error) return response.status(500).send(error);
        console.log(user);
        request.session.user = user;
        response.send({response: user})
      });

  });
});

//User signout
router.get('/user-out', function (request, response) {
  request.session.destroy(function (error) {
    if (error) {
      response.send({response: false});
    } else {
      response.send({response: true});
    }
  });
});

module.exports = router;