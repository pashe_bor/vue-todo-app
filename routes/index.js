var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Органайзер' })
});

router.get('/create-todo', function (req, res, next) {
  if(!req.session.user) {
    res.redirect('/');
  } else {
    res.render('index', { title: 'Органайзер | Заметки' })
  }
});

router.get('/currency-converter', function (req, res, next) {
  if(!req.session.user) {
    res.redirect('/');
  } else {
    res.render('index', { title: 'Органайзер | Конвертор валют' })
  }
});

router.get('/chat', function (req, res, next) {
  if(!req.session.user) {
    res.redirect('/');
  } else {
    res.render('index', { title: 'Органайзер | Чат' })
  }
});

router.get('/todo/:id', function (req, res, next) {
    res.redirect('/');
});


module.exports = router;
