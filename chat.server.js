const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);


server.listen(8080, function () {
  console.log('Chat is on port 8080');
});

io.sockets.on('connection', function (socket) {
  socket.on('room', function(room) {
    socket.join(room);
  });
  
  socket.on('sendMessage', function (data) {
    console.log(data);
    io.sockets.emit('customEmit', data);
  });
});