import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const state = {
  messages: []
}

const mutations = {
  SET_NEW_MESSAGE: (state, message) => {
    state.messages.push(message)
  }
}

const actions = {
  setMessage: ({commit}, message) => {
    commit('SET_NEW_MESSAGE', message)
  }
}

const getters = {

}

export default {
  state, mutations, actions, getters
}
