import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const state = {
  todos: [],
  notification: {
    title: '',
    message: '',
    isActive: false
  },
  todosFilter: ''
}

const mutations = {
  addTodo: (state, payload) => {
    state.todos.unshift(payload)
  },
  deleteTodo: (state, id) => {
    let index = state.todos.findIndex(todo => todo._id === id)
    state.todos.splice(index, 1)
  },
  editTodo: (state, payload) => {
    state.todos.forEach(item => {
      if (item._id === payload._id) {
        item.title = payload.title
        item.text = payload.text
        item.importance = payload.importance
      }
    })
  },
  CLEAR_STORE_SUCCESS: (state) => {
    state.todos = []
  },
  GET_TODOS_SUCCESS: (state, payload) => {
    state.todos = payload
  },
  NOTIFICATION_OPEN: (state, payload) => {
    state.notification = payload
  },
  NOTIFICATION_CLOSE: (state) => {
    state.notification.isActive = false
  },
  TODOS_FILTER: (state, sign) => {
    state.todosFilter = sign
  }
}

const actions = {
  clearStoreAction: ({commit}) => {
    commit('CLEAR_STORE_SUCCESS')
  },
  showNotification: ({commit}, data) => {
    commit('NOTIFICATION_OPEN', data)
  },
  closeNotification: ({commit}, isActive) => {
    commit('NOTIFICATION_CLOSE', isActive)
  },
  searchAction: ({commit}, value) => {
    commit('TODOS_FILTER', value)
  },
  add: ({commit}, data) => {
    return Vue.http.put('/todos', data)
      .then(json => {
        let response = json.body.response
        commit('addTodo', response)
        commit('NOTIFICATION_OPEN', {
          title: 'Добавление записи',
          message: `Запись "${response.title}" добавлена.`,
          isActive: true
        })
      })
  },
  edit: ({commit}, formData) => {
    return Vue.http.post('/todos', formData)
      .then(json => {
        let todo = json.body.response
        commit('editTodo', todo)
        commit('NOTIFICATION_OPEN', {
          title: 'Удаление записи',
          message: `Запись #${todo._id} изменена`,
          isActive: true
        })
      })
  },
  removeTodo: ({commit}, id) => {
    return Vue.http.delete(`/todos/${id}`)
      .then(json => {
        let deletedTodo = json.body.response
        commit('NOTIFICATION_OPEN', {
          title: 'Удаление записи',
          message: `Запись #${deletedTodo.title} удалена`,
          isActive: true
        })
        commit('deleteTodo', deletedTodo._id)
      })
  },
  getTodos: ({commit}) => {
    return Vue.http.get('/todos')
      .then(json => {
        const response = json.body.response
        commit('GET_TODOS_SUCCESS', response)
      })
  }
}

const getters = {
  getTodoById: (state) => (id) => {
    return state.todos.find(todo => todo._id === id)
  }
}

export default {
  state, mutations, actions, getters
}
