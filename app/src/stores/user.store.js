import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const state = {
  userInfo: {},
  access: undefined,
  isAuthorize: false,
  notification: {
    title: '',
    message: '',
    isActive: ''
  }
}

const mutations = {
  SET_ACCESS: (state, access) => {
    state.access = access
  },
  SET_AUTHORIZATION: (state, isAuthorize) => {
    state.isAuthorize = isAuthorize
  },
  SET_USER: (state, user) => {
    state.userInfo = user
  },
  SET_NOTIFICATION: (state, notification) => {
    state.notification = notification
  },
  EDIT_USER_SUCCES: (state, userData) => {
    state.userInfo.name = userData.name
    state.userInfo.avatar = userData.avatar
    state.userInfo.email = userData.email
  }
}

const actions = {
  setAccess: ({commit}, access) => {
    commit('SET_ACCESS', access)
  },
  closeNotificationLogin: ({commit}, data) => {
    commit('SET_NOTIFICATION', data)
  },
  editUser: ({commit}, userData) => {
    return Vue.http.post('/user-update', userData, {emulateJSON: true})
      .then(json => {
        let response = json.body.response
        commit('SET_USER', response)
        commit('SET_NOTIFICATION', {
          title: 'Изменение данных пользователя',
          message: 'Данные изменены успешно',
          isActive: true
        })
      }).catch(error => {
        console.log(error)
      })
  },
  exitUser: ({commit}) => {
    return Vue.http.get('/user-out')
      .then(json => {
        let response = json.body.response
        if (response) {
          commit('SET_ACCESS', false)
          commit('SET_USER', {})
        }
      }).catch(error => {
        console.log(error)
      })
  },
  registerUser: ({commit}, user) => {
    return Vue.http.put('/users', user, {emulateJSON: true})
      .then(json => {
        const response = json.body.response
        if (!response) {
          commit('SET_NOTIFICATION', {
            title: 'Регистрация',
            message: 'Пароли не совпадают! Введите корректно пароль/подтверждение пароля',
            isActive: true
          })
        } else {
          commit('SET_NOTIFICATION', {
            title: 'Регистрация',
            message: 'Вы успешо зарегистрировались! Теперь перейдите на форму авторизации и автризуйтесь.',
            isActive: true
          })
        }
      })
  },
  setSuccessAuthorization: ({commit}, author) => {
    return Vue.http.post('users', author)
      .then(json => {
        const response = json.body.response
        if (response) {
          commit('SET_USER', response)
          commit('SET_AUTHORIZATION', true)
          commit('SET_NOTIFICATION', {
            title: 'Авторизация',
            message: 'Вы успешно авторизовались',
            isActive: true
          })
        } else {
          commit('SET_NOTIFICATION', {
            title: 'Авторизация',
            message: 'Вы не прваильно ввели логин/пароль!',
            isActive: true
          })
        }
      })
  },
  getAuthorizedUser: ({commit}) => {
    return Vue.http.get('/users')
      .then(json => {
        const response = json.body.response
        if (response) {
          commit('SET_USER', response)
          commit('SET_ACCESS', true)
        } else {
          commit('SET_ACCESS', false)
        }
      })
  }
}

const getters = {
  getUserData: state => state.userInfo
}

export default {
  state, mutations, actions, getters
}
