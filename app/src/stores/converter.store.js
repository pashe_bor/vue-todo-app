import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const state = {
  value: '',
  currencies: false,
  loading: false
}

const mutations = {
  SET_CONVERT_VALUE_SUCCESS: (state, payload) => {
    state.value = payload
  },
  GET_CURRENCIES_SUCCESS: (state, payload) => {
    state.currencies = payload
  },
  SET_LOADING: (state, value) => {
    state.loading = value
  }
}

const actions = {
  getCurrencies: ({commit}) => {
    commit('SET_LOADING', true)
    return Vue.http.get('https://free.currencyconverterapi.com/api/v5/currencies')
      .then(json => {
        let response = json.body
        commit('GET_CURRENCIES_SUCCESS', response.results)
        commit('SET_LOADING', false)
      }).catch(error => console.log(error))
  },
  getConvertedValue: ({commit}, formData) => {
    commit('SET_LOADING', true)
    return Vue.http.get(`https://free.currencyconverterapi.com/api/v5/convert?q=${formData.from}_${formData.to}&compact=y`)
      .then(json => {
        let result = json.body[`${formData.from}_${formData.to}`]
        let symbol = state.currencies[`${formData.to}`].currencySymbol
        commit('SET_CONVERT_VALUE_SUCCESS', (result.val.toFixed(2) * formData.nominal) + ' ' + symbol)
        commit('SET_LOADING', false)
      }).catch(error => console.log(error))
  }
}

const getters = {
}

export default {
  state, mutations, actions, getters
}
