import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const state = {
  animals: []
}

const mutations = {
  setAnimals: (state, payload) => {
    state.animals = payload
  }
}

const actions = {
  getAnimals: ({commit}) => {
    return Vue.http.get('https://jsonplaceholder.typicode.com/photos')
      .then(json => {
        commit('setAnimals', json.body)
      })
  }
}

export default {
  state, mutations, actions
}
