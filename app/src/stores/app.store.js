import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const state = {
  title: ''
}

const mutations = {
  SET_COMPONENT_TITLE: (state, title) => {
    state.title = title
  }
}

const actions = {
  setComponentTitle: ({commit}, title) => {
    commit('SET_COMPONENT_TITLE', title)
  }
}

const getters = {

}

export default {
  state, mutations, actions, getters
}
