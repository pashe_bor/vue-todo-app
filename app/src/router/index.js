import Vue from 'vue'
import Router from 'vue-router'
import Main from '../components/Main'
import EditTodo from '../components/todos/EditTodo'
import AnimalGalery from '../components/animals-gallery/AnimalGalery'
import CreateTodo from '../components/todos/CreateTodo'
import Register from '../components/autorization/Register'
import CurrencyConverter from '../components/currency-converter/CurrencyConverter'
import Chat from '../components/chat/Chat'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/todo/:id',
      name: 'edit-todo',
      component: EditTodo
    },
    {
      path: '/create-todo/',
      name: 'create-todo',
      component: CreateTodo
    },
    {
      path: '/animals-gallery',
      name: 'animals-gallery',
      component: AnimalGalery
    },
    {
      path: '/currency-converter',
      name: 'currency-converter',
      component: CurrencyConverter
    },
    {
      path: '/chat',
      name: 'chat',
      component: Chat
    }
  ]
})
