/* eslint-disable no-new */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import VueSocketio from 'vue-socket.io'
import animalsGalleryStore from './stores/animalsGalleryStore'
import todosStore from './stores/todosStore'
import userStore from './stores/user.store'
import converterStore from './stores/converter.store'
import appStore from './stores/app.store'
import chatStore from './stores/chat.store'

Vue.use(Vuex)
Vue.use(VueSocketio, 'localhost:8080')
Vue.config.productionTip = false

const store = new Vuex.Store({
  modules: {
    animalsGalleryStore,
    todosStore,
    userStore,
    converterStore,
    appStore,
    chatStore
  }
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store: store
})
