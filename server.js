const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const path = require('path');
const app = express();

//DB options
const options = {
  user: 'pashebor',
  pass: 'ltvmzyjd90',
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0
};
//Connecting to the db
mongoose.connect('mongodb://localhost:27017/todo_db?authSource=admin', options);

const routes = require('./routes/index');
const staticFiles = './app/dist/static';
const uploads = './uploads';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//Register a session with it's id
app.use(session({
  secret: 'asdas0d08as0d80a',
  cookie:{maxAge: 24*60*60*1000},
  proxy: true,
  resave: true,
  saveUninitialized: true
}));

app.use('/static', express.static(staticFiles));
app.use('/', routes);
app.use('/', express.static(uploads))
app.use('/', require('./routes/users'));
app.use('/', require('./routes/todos'));
app.use('/register', routes);



const server = app.listen(8080, function () {
    console.log('Organizer application is listening on port 80!');
});


const io = require('socket.io')(server);

io.sockets.on('connection', function (socket) {
  socket.on('room', function(room) {
    socket.join(room);
  });
  
  socket.on('sendMessage', function (data) {
    console.log(data);
    io.sockets.emit('customEmit', data);
  });
});