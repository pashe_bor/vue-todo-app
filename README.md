# vue-test-app

> A Vue.js project

## How to run a server
1. You need to install a mongoDB
2. Upload a test data (backup directory at the root of the project) collections to a mongoDB via command 'mongorestore backup'
3. Run npm script 'start-dev-server'

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```