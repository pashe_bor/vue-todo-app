const dateFormat = function (date) {
  let dd = date.getDate(),
  	mm = date.getMonth() + 1,
		yy = date.getFullYear() % 100,
		HH = date.getHours(),
		MM = date.getMinutes()

  if (dd < 10) dd = '0' + dd
  if (mm < 10) mm = '0' + mm
  if (yy < 10) yy = '0' + yy
  if (HH < 10) HH = '0' + HH
  if (MM < 10) MM = '0' + MM

  return dd + '.' + mm + '.' + yy + ' ' + HH + ':' + MM
}

module.exports = dateFormat
