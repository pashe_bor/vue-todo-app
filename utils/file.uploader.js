const multer = require('multer');

const storage = multer.diskStorage({
  destination: './uploads',
  filename: function (req, file, callback) {
    callback(null, Date.now() + '_' + file.originalname)
  }
})

const upload = multer({storage: storage}).single('avatar')

module.exports = upload